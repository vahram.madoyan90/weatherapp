//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 19.09.21.
//

import Foundation

final class WeatherModel: Decodable {
    let timezone: String
    let current: CurrentWeather
    let daily: [DailyWeather]
}

final class CurrentWeather: Decodable {
    let dt: Double
    let temp: Double
    let feelsLike: Double
    let weather: [Weather]
}

final class Weather: Decodable {
    let main: String
    let description: String
    let icon: String
    let id: Int
}

final class DailyWeather: Decodable {
    let dt: Double
    let temp: Temp
    let weather: [Weather]
}

final class Temp: Decodable {
    let day: Double
    let min: Double
    let max: Double
    let night: Double
    let eve: Double
    let morn: Double
}
