//
//  Notification.Name+Extension.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 20.09.21.
//

import Foundation

extension Notification.Name {
    static let dataLoaded = Notification.Name(rawValue: "dataLoaded")
}
