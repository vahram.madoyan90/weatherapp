//
//  UIView+Extension.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 20.09.21.
//

import UIKit

extension UIView {
    func addSubviewWithLayoutToBounds(subview: UIView,
                                      insets: UIEdgeInsets = .zero,
                                      isForSafeArea: Bool = false) {
        addSubview(subview)
        addConstraintToBounds(subView: subview,
                              insets: insets,
                              isForSafeArea: isForSafeArea)
    }
    
    private func addConstraintToBounds(subView: UIView,
                                       insets: UIEdgeInsets,
                                       isForSafeArea: Bool) {

        subView.translatesAutoresizingMaskIntoConstraints = false
        subView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: insets.left).isActive = true
        subView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -insets.right).isActive = true

        if #available(iOS 11, *), isForSafeArea {
            let guide = safeAreaLayoutGuide
            subView.topAnchor.constraint(equalTo: guide.topAnchor, constant: insets.top).isActive = true
            subView.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: -insets.right).isActive = true
        } else {
            subView.topAnchor.constraint(equalTo: topAnchor, constant: insets.top).isActive = true
            subView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -insets.bottom).isActive = true
        }
    }
}

extension UIResponder {
    static var id: String {
        return String(describing: self)
    }
}
