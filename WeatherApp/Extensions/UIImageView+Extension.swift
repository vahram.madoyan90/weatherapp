//
//  UIImageView+Extension.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 20.09.21.
//

import UIKit

extension UIImageView {
    private func downloaded(from url: URL) {
        let loader = Loader.showOn(self)
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                loader.remove()
                return
            }
            DispatchQueue.main.async() { [weak self] in
                loader.remove()
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url)
    }
}
