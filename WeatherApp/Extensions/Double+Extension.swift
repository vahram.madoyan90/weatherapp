//
//  Double+Extension.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 19.09.21.
//

import Foundation

extension Double {
    var unixToDatet: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
//        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
//        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.dateFormat = "dd/MM/yy"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: date)
    }
}
