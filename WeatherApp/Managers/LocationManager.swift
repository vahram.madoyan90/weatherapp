//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 19.09.21.
//

import Foundation
import CoreLocation

final class LocationManager: NSObject {
    
    // MARK: - Methods

    static func getWeatherWithLocation<T: Decodable>(type: T.Type, longitude: Double? = nil, latitude: Double? = nil, completion: @escaping(T?) -> Void) {
        if longitude == nil || latitude == nil {
            getIpLocation { data in
                if let dict = data,
                   let longitude = dict["lon"] as? Double,
                   let latitude = dict["lat"] as? Double {
                    RequestController.objectWithCompletion(type: T.self, url: API.weatherWithLocatiom(longitude: longitude, latitude: latitude)) { model in
                        completion(model)
                    }
                }
            }
        } else {
            RequestController.objectWithCompletion(type: T.self, url: API.weatherWithLocatiom(longitude: longitude!, latitude: latitude!)) { model in
                completion(model)
            }
        }
    }
    
    // MARK: - Private Methods
    
    static func getIpLocation(completion: @escaping([String: Any]?) -> Void) {
        let url     = URL(string: "http://ip-api.com/json")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler:
                                    { (data, response, error) in
                                        DispatchQueue.main.async
                                        {
                                            if let content = data
                                            {
                                                do
                    {
                        if let object = try JSONSerialization.jsonObject(with: content, options: .allowFragments) as? [String: Any]
                        {
                            completion(object)
                        }
                        else
                        {
                            completion(nil)
                        }
                    }
                                                catch
                                                {
                                                    completion(nil)
                                                }
                                            }
                                            else
                                            {
                                                completion(nil)
                                            }
                                        }
                                    }).resume()
    }
}
