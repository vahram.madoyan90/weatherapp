//
//  Loader.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 20.09.21.
//

import UIKit

final class Loader: UIView {
    static func showOn(_ superView: UIView, topInset: CGFloat = 0) -> Loader {
        let loader = Loader()
        superView.addSubviewWithLayoutToBounds(subview: loader, insets: UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0))
        loader.backgroundColor = .white
        loader.addActivityIndicator()
        return loader
    }

    func remove() {
        removeFromSuperview()
    }

    private func addActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityIndicator.color = .gray
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.startAnimating()
    }
}
