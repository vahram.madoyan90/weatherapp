//
//  MainViewController.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 17.09.21.
//

import UIKit
import CoreLocation

class MainViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    // MARK: - Properties
    
    private let locationManager = CLLocationManager()
    private var longitude: Double?
    private var latitude: Double?
    private var loader: Loader?
    
    
    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocation()
    }
    
    // MARK: - Private Methods
    
    private func configureView() {
        if let model = UIApplication.appDelegate.weatherModel {
            let currentWeather = model.current
            self.cityLabel.text = model.timezone
            self.iconImageView.downloaded(from: API.weatherImage(currentWeather.weather[0].icon))
            self.tempLabel.text = "Temp \(currentWeather.temp.rounded())"
            self.feelsLikeLabel.text = "Feels Like \(currentWeather.feelsLike.rounded())"
            self.descriptionLabel.text = currentWeather.weather[0].description.uppercased()
        }
    }
    
    private func setupLocation() {
        showLoaderIfNeeded()
        if CLLocationManager.locationServicesEnabled() {
            if locationManager.authorizationStatus != .notDetermined {
                longitude = locationManager.location?.coordinate.longitude
                latitude = locationManager.location?.coordinate.latitude
                getWeather(longitude: longitude, latitude: latitude)
            } else {
                locationManager.delegate = self
                locationManager.requestWhenInUseAuthorization()
            }
        } else {
            getWeather()
        }
    }
   
    private func getWeather(longitude: Double? = nil, latitude: Double? = nil) {
        LocationManager.getWeatherWithLocation(type: WeatherModel.self, longitude: longitude, latitude: latitude) { [weak self] response in
            guard let self = self else { return }
            self.loader?.remove()
            self.loader = nil
            if let model = response {
                UIApplication.appDelegate.updateWeatherModel(model)
                self.configureView()
            } else {
                self.showAlert()
            }
        }
    }
    
    private func showAlert() {
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            self?.getWeather(longitude: self?.longitude, latitude: self?.latitude)
        }
        let alertController = UIAlertController(title: "Something Went Wrong", message: "Please Try Again", preferredStyle: .alert)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func showLoaderIfNeeded() {
        if loader == nil {
            loader = Loader.showOn(view)
        }
    }
}


extension MainViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        longitude = manager.location?.coordinate.longitude
        latitude = manager.location?.coordinate.latitude
        getWeather(longitude: longitude, latitude: latitude)
    }
}
