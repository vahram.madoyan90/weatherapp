//
//  ForecastViewController.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 20.09.21.
//

import UIKit

class ForecastViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    private var dataSourse: [DailyWeather] = []
    
    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 12, right: 0)
        setupData()
    }
    
    // MARK: - Private Methods
    
    private func setupData() {
        if let model = UIApplication.appDelegate.weatherModel {
            dataSourse = model.daily
            tableView.reloadData()
        }
    }
}

extension ForecastViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourse.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ForecastTableViewCell.id) as! ForecastTableViewCell
        cell.configureCellWithModel(dataSourse[indexPath.row])
        return cell
    }

}
