//
//  ForecastTableViewCell.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 20.09.21.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
    
    // MARK: - Outlets

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    func configureCellWithModel(_ model: DailyWeather)  {
        iconImageView.downloaded(from: API.weatherImage(model.weather[0].icon))
        dayLabel.text = model.dt.unixToDatet
        tempLabel.text = "Temp \(model.temp.day.rounded())"
        minTempLabel.text = "Min temp \(model.temp.min.rounded())"
        maxTempLabel.text = "Max temp \(model.temp.max.rounded())"
        descriptionLabel.text = model.weather[0].description.uppercased()
    }

}
