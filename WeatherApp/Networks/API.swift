//
//  API.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 19.09.21.
//

import Foundation

struct API {
    
//    "https://api.openweathermap.org/data/2.5/weather?q=London&appid=756d265bc25d8d8efcb7795850a52661"
    static let base = "https://api.openweathermap.org/"
    static let id = "756d265bc25d8d8efcb7795850a52661"
    
    static func weatherWithCity(_ city: String) -> String {
        return "\(base)data/2.5/2.5/onecall?q=\(city)&appid=\(id)&exclude=minutely,hourly&units=metric"
//        return "\(base)data/2.5/weather?q=\(city)&appid=\(id)&units=metric"
    }
    
    static func weatherWithLocatiom(longitude: Double, latitude: Double) -> String {
        return "\(base)data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&exclude=minutely,hourly&appid=\(id)&units=metric"
        
    }
    
    static func weatherImage(_ image: String) -> String {
        return "\(base)img/w/\(image).png"
    }
    
//    api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API key}
//    https://api.openweathermap.org/data/2.5/onecall?lat=37.7858&lon=-122.4064&exclude=daily&appid=756d265bc25d8d8efcb7795850a52661
}
