//
//  RequestController.swift
//  WeatherApp
//
//  Created by Vahram Madoyan on 19.09.21.
//

import Foundation
import Alamofire

final class RequestController {
    
    static func objectWithCompletion<T: Decodable>(type: T.Type, url: String, httpMethod: HTTPMethod = .get, _ completion: @escaping (T?) -> Void){
        AF.request(url, method: httpMethod).responseJSON { response in
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let model = try decoder.decode(type, from: data)
                    completion(model)
                } catch {
                    completion(nil)
                }
            }
        }
    }
}
